package _2022.days

import readInput

fun main() {


    fun String.readCommand(): ReadCommandResult {
        val split = split(" ")
        val command = split[1]
        return if (command == "cd") {
            when (val destination = split[2]) {
                ".." -> ReadCommandResult.GoUp
                "/" -> ReadCommandResult.GoToRoot
                else -> ReadCommandResult.GoToDestination(destination)
            }
        } else {
            ReadCommandResult.ListContent
        }
    }

    fun Directory.getDirectoriesSmallerThanOrEqualSize(size: Long): List<Directory> {
        val filteredDirectories = mutableListOf<Directory>()
        if (calculateSize() <= size) filteredDirectories.add(this)
        val innerDirectoriesSmallerThanSize =
            innerPlaces.filterIsInstance<Directory>().map { it.getDirectoriesSmallerThanOrEqualSize(size) }.flatten()
        filteredDirectories.addAll(innerDirectoriesSmallerThanSize)
        return filteredDirectories
    }

    fun Directory.getDirectoriesGraterThanOrEqualSize(size: Long): List<Directory> {
        val filteredDirectories = mutableListOf<Directory>()
        if (calculateSize() >= size) filteredDirectories.add(this)
        val innerDirectoriesSmallerThanSize =
            innerPlaces.filterIsInstance<Directory>().map { it.getDirectoriesGraterThanOrEqualSize(size) }.flatten()
        filteredDirectories.addAll(innerDirectoriesSmallerThanSize)
        return filteredDirectories
    }

    fun initializeDirectoriesTree(input: List<String>): Directory {
        val root = Directory("/", null)
        var currentDirectory = root
        var currentLine = 0
        while (currentLine < input.size) {
            when (val commandResult = input[currentLine].readCommand()) {
                is ReadCommandResult.GoToDestination ->
                    currentDirectory =
                        currentDirectory.innerPlaces.find { it.name == commandResult.directoryName } as? Directory
                            ?: error("This directory not defined in this level before")

                ReadCommandResult.GoToRoot -> currentDirectory = root
                ReadCommandResult.GoUp -> currentDirectory =
                    currentDirectory.parent ?: root
                ReadCommandResult.ListContent
                -> {
                    currentLine++
                    while (currentLine < input.size) {
                        val currentPlace = input[currentLine].split(" ")
                        if (currentPlace.first() == "$") {
                            currentLine--
                            break
                        } else if (currentPlace.first() == "dir") {
                            currentDirectory.innerPlaces.add(Directory(currentPlace.last(), currentDirectory))
                        } else {
                            currentDirectory.innerPlaces.add(File(currentPlace.first().toLong(), currentPlace.last()))
                        }
                        currentLine++
                    }
                }
            }
            currentLine++
        }
        return root
    }

    fun part1(input: List<String>): Long {
        val root = initializeDirectoriesTree(input)

        return root.getDirectoriesSmallerThanOrEqualSize(100000).sumOf { it.calculateSize() }
    }

    fun part2(input: List<String>): Long {
        val root = initializeDirectoriesTree(input)
        val usedSpace = root.calculateSize()
        val neededSpace = usedSpace-40000000
        return root.getDirectoriesGraterThanOrEqualSize(neededSpace).minByOrNull { it.calculateSize() }!!.calculateSize()
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day7", 2022)
    println(part1(testInput))
    println(part2(testInput))
}


sealed class ReadCommandResult {
    object GoUp : ReadCommandResult()
    class GoToDestination(val directoryName: String) : ReadCommandResult()
    object GoToRoot : ReadCommandResult()
    object ListContent : ReadCommandResult()
}

interface Place {
    var name: String

    fun calculateSize(): Long
}

class Directory(override var name: String, val parent: Directory?) : Place {
    val innerPlaces = mutableListOf<Place>()
    override fun calculateSize(): Long {
        return innerPlaces.sumOf { it.calculateSize() }
    }

    override fun toString(): String {
        return """
            $name: {
               ${innerPlaces.map { it.toString() }}
            }
        """.trimIndent()
    }
}

data class File(val size: Long, override var name: String) : Place {
    override fun calculateSize(): Long = size
}