package _2022.days

import readInput
import java.util.*

fun main() {
    fun getTheFirstDistinctSubstring(input: List<String>,size:Int): Int {
        val indexOfFirstDistinctString = input.first().windowed(size) { subString ->
            val isDistinct = subString.toString().toCharArray().distinct().size == subString.length
            isDistinct
        }.indexOfFirst { it }
        return indexOfFirstDistinctString
    }

    fun part1(input: List<String>): Int {
        val indexOfFirstDistinctString = getTheFirstDistinctSubstring(input,4)
        return indexOfFirstDistinctString + 4
    }

    fun part2(input: List<String>): Int {
        val indexOfFirstDistinctString = getTheFirstDistinctSubstring(input,14)
        return indexOfFirstDistinctString + 14
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day6", 2022)
    println(part1(testInput))
    println(part2(testInput))
}