package _2022.days

import readInput
import java.util.*

fun main() {

    fun part1(input: List<String>): String {
        return ""
    }

    fun part2(input: List<String>): String {
        return ""
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day2", 2022)
    println(part1(testInput))
    println(part2(testInput))
}
