package _2022.days

import readInput
import java.util.*

fun main() {
    fun readInput(
        input: List<String>,
        cratesLineIndex: Int
    ): Array<Stack<Char>> {
        val cratesCount = input[cratesLineIndex].substringAfterLast("   ").dropLast(1).toInt()
        val cratesStacks = Array(cratesCount) { Stack<Char>() }
        for (lineIndex in cratesLineIndex - 1 downTo 0) {
            val filterIndexed = input[lineIndex].drop(1).filterIndexed { index, char ->
                index % 4 == 0
            }
            filterIndexed.forEachIndexed { index, c ->
                if (!c.isWhitespace()) {
                    cratesStacks[index].push(c)
                }
            }
        }
        return cratesStacks
    }

    fun doInstructions(
        instructionsRange: IntRange,
        input: List<String>,
        cratesStacks: Array<Stack<Char>>,
        keepItemsOrder: Boolean,
    ) {
        for (instructionIndex in instructionsRange) {
            input[instructionIndex].split(" ").apply {
                val quantity = get(1).toInt()
                val source = get(3).toInt()
                val destination = get(5).toInt()
                val poppedUpItems = cratesStacks[source - 1].popQuantity(quantity)
                if (keepItemsOrder) {
                    poppedUpItems.reversed().forEach {
                        cratesStacks[destination - 1].push(it)
                    }
                } else {
                    poppedUpItems.forEach {
                        cratesStacks[destination - 1].push(it)
                    }
                }
            }
        }
    }

    fun part1(input: List<String>): String {
        val cratesLineIndex = input.indexOfFirst { it.contains('1') }
        val cratesStacks = readInput(input, cratesLineIndex)
        val instructionsRange = (cratesLineIndex + 2) until input.size
        doInstructions(instructionsRange, input, cratesStacks, false)
        return cratesStacks.joinToString("") { it.peek().toString() }
    }

    fun part2(input: List<String>): String {
        val cratesLineIndex = input.indexOfFirst { it.contains('1') }
        val cratesStacks = readInput(input, cratesLineIndex)
        val instructionsRange = (cratesLineIndex + 2) until input.size
        doInstructions(instructionsRange, input, cratesStacks, true)
        return cratesStacks.joinToString("") { it.peek().toString() }
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day5", 2022)
    println(part1(testInput))
    println(part2(testInput))
}

fun <T> Stack<T>.popQuantity(quantity: Int): List<T> {
    val result = mutableListOf<T>()
    repeat(quantity) {
        val poppedItem = pop()
        if (poppedItem == null) return@repeat
        else {
            result.add(poppedItem)
        }
    }
    return result
}
