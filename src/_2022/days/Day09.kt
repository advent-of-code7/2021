package _2022.days

import readInput
import java.awt.Point

fun main() {

    fun handleMoves(
        direction: Char,
        steps: Int,
        points: List<Point>,
        visitedPositions: MutableSet<Pair<Int, Int>>,
    ) {

    }

    fun part1(input: List<String>): Int {

        val visitedPositions = mutableSetOf<Pair<Int, Int>>(0 to 0)
        val head = Point(0, 0)
        val tail = Point(0, 0)
        input.forEach {
            val direction = it.first()
            val steps = it.split(" ").last().toInt()
            handleMoves(direction, steps, listOf(head, tail), visitedPositions)
        }

        return visitedPositions.size
    }

    fun part2(input: List<String>): Int {

        val visitedPositions = mutableSetOf<Pair<Int, Int>>(0 to 0)
        val head = Point(0, 0)
        val points = listOf(head).plus(Array(9) { Point(0, 0) })
        input.forEach {
            val direction = it.first()
            val steps = it.split(" ").last().toInt()
            handleMoves(direction, steps, points, visitedPositions)
        }

        return visitedPositions.size
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day9", 2022)
    println(part1(testInput))
    println(part2(testInput))
}
