package _2022.days

import MOD
import readInput
import java.math.BigInteger

fun main() {

    fun getMonkeys(input: List<String>) = input.chunked(7).mapIndexed { index, strings ->
        val startingItems =
            strings[1].substringAfter("Starting items: ").split(", ").map { it.toLong() }.toMutableList()
        val operationValue = strings[2].split(" ").last().toLongOrNull() ?: -1L
        val operation = if (strings[2].contains("*")) {
            Monkey.Operation.Multiply(operationValue)
        } else {
            Monkey.Operation.Sum(operationValue)
        }
        val conditionNumber = strings[3].split(" ").last().toLong()
        val conditionTrueMonkey = strings[4].split(" ").last().toLong()
        val conditionFalseMonkey = strings[5].split(" ").last().toLong()

        Monkey(
            startingItems,
            operation,
            { worryLeveL -> worryLeveL % conditionNumber == 0L },
            conditionTrueMonkey, conditionFalseMonkey
        )
    }

    fun part1(input: List<String>): Long {
        val monkeys = getMonkeys(input)
        repeat(20) {
            monkeys.forEach { monkey ->
                monkey.inspectItems(monkeys, isWorryDividedByThree = true)
            }
        }

        return monkeys.sortedByDescending { it.inspectsCount }.take(2)
            .fold(1L) { acc, monkey -> acc * monkey.inspectsCount }
    }

    fun part2(input: List<String>): Long {
        val monkeys = getMonkeys(input)
        repeat(10000) {
            monkeys.forEach {
                it.inspectItems(monkeys, false)
            }
        }
        return monkeys.sortedByDescending { it.inspectsCount }.take(2)
            .fold(1L) { acc, monkey -> acc * monkey.inspectsCount }
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day11", 2022)
    println(part1(testInput))
    println(part2(testInput))
}

class Monkey(
    val currentItems: MutableList<Long>,
    val operation: Operation,
    val condition: (worryLeveL: Long) -> Boolean,
    private val conditionTrueMonkey: Long,
    private val conditionFalseMonkey: Long,
) {

    var inspectsCount = 0L

    private fun getDestinationMonkey(currentWorryLevel: Long): Long =
        if (condition(currentWorryLevel)) conditionTrueMonkey else conditionFalseMonkey

    fun inspectItems(monkeys: List<Monkey>, isWorryDividedByThree: Boolean) {
        currentItems.forEach { itemWorryLeve ->
            val newValue = (if (operation.value == -1L) {
                when (operation) {
                    is Operation.Multiply -> itemWorryLeve * itemWorryLeve
                    is Operation.Sum -> itemWorryLeve + itemWorryLeve
                }
            } else {
                when (operation) {
                    is Operation.Multiply -> itemWorryLeve * operation.value
                    is Operation.Sum -> itemWorryLeve + operation.value
                }
            } % MOD) / if (isWorryDividedByThree) 3L else 1L
            val monkeyIndex = getDestinationMonkey(newValue)
            monkeys[monkeyIndex.toInt()].currentItems += (newValue % MOD)
            inspectsCount++
        }
        currentItems.clear()
    }

    sealed class Operation(val value: Long) {

        class Sum(value: Long) : Operation(value)

        class Multiply(value: Long) : Operation(value)

    }
}