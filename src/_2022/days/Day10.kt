package _2022.days

import readInput
import kotlin.math.abs

fun main() {


    fun getCycles(input: List<String>) = input.map {
        val split = it.split(" ")
        if (split.first() == "addx") {
            Operation.Adding(split.last().toInt())
        } else {
            Operation.NoOperation
        }
    }.flatMap {
        when (it) {
            is Operation.Adding -> {
                listOf(
                    Cycle(0),
                    Cycle(it.value)
                )
            }

            Operation.NoOperation -> {
                listOf(Cycle(0))
            }
        }
    }

    fun part1(input: List<String>): Int {

        val cycles = getCycles(input)

        val xValues = (cycles).runningFold(1) { acc, cycle ->
            acc + cycle.nextCycleAddedValue
        }
        val indicesForSum = listOf(20, 60, 100, 140, 180, 220)

        return indicesForSum.sumOf { xValues[it - 1] * it }
    }

    fun part2(input: List<String>): String {
        val cycles = getCycles(input)
        val xValues = (cycles).runningFold(1) { acc, cycle ->
            acc + cycle.nextCycleAddedValue
        }
        val resultCRT = cycles.zip(xValues).foldIndexed("") { index, acc, pair ->
            acc + if (abs((index % 40) - pair.second) < 2) "#"
            else "."
        }.chunked(40).joinToString("\n")
        return resultCRT
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day10", 2022)
    println(part1(testInput))
    println(part2(testInput))
}

sealed class Operation {
    object NoOperation : Operation()

    class Adding(val value: Int) : Operation()
}

data class Cycle(val nextCycleAddedValue: Int)