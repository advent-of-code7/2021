package _2022.days

import readInput

fun main() {

    fun isOnTheSide(matrix: Array<Array<Int>>, row: Int, col: Int) =
        row == 0 || col == 0 || row == matrix.first().size - 1 || col == matrix.size - 1

    fun part1(input: List<String>): Int {
        val grid = Array(input.size) { row ->
            input[row].toCharArray().map { it.digitToInt() }.toTypedArray()
        }
        val treeGrid = grid.mapIndexed { row, ints ->
            ints.mapIndexed { col, height ->
                Tree(row, col, height, isOnTheSide(grid, row, col))
            }.toTypedArray()
        }.toTypedArray()

        var visibleCount = 0
        treeGrid.forEach { treesRow ->
            treesRow.forEach { tree ->
                if (tree.isVisible(treeGrid)) {
                    visibleCount++
                }
            }
        }
        return visibleCount
    }

    fun part2(input: List<String>): Int {
        val grid = Array(input.size) { row ->
            input[row].toCharArray().map { it.digitToInt() }.toTypedArray()
        }
        val treeGrid = grid.mapIndexed { row, ints ->
            ints.mapIndexed { col, height ->
                Tree(row, col, height, isOnTheSide(grid, row, col))
            }.toTypedArray()
        }.toTypedArray()
        val maxOfScenicScore = treeGrid.maxOf {
            it.maxOf {
                it.getScenicScore(treeGrid)
            }
        }
        return maxOfScenicScore
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day8", 2022)
    println(part1(testInput))
    println(part2(testInput))
}

data class Tree(val row: Int, val col: Int, val height: Int, val isOnTheSide: Boolean)

fun Tree.isVisible(grid: Array<Array<Tree>>) =
    isVisibleFromDown(grid, height)
            || isVisibleFromUp(grid, height)
            || isVisibleFromRight(grid, height)
            || isVisibleFromLeft(grid, height)

fun Tree.getScenicScore(grid: Array<Array<Tree>>) =
    (getDownDistance(grid, height)) *
            (getUpDistance(grid, height)) *
            (getRightDistance(grid, height)) *
            (getLeftDistance(grid, height))

fun Tree.isVisibleFromRight(grid: Array<Array<Tree>>, height: Int): Boolean {
    if (isOnTheSide) return true
    val rightTree = grid[row][col + 1]
    return rightTree.isVisibleFromRight(grid, height) && height > rightTree.height
}

fun Tree.isVisibleFromLeft(grid: Array<Array<Tree>>, height: Int): Boolean {
    if (isOnTheSide) return true
    val leftTree = grid[row][col - 1]
    return leftTree.isVisibleFromLeft(grid, height) && height > leftTree.height
}

fun Tree.isVisibleFromUp(grid: Array<Array<Tree>>, height: Int): Boolean {
    if (isOnTheSide) return true
    val upTree = grid[row - 1][col]
    return upTree.isVisibleFromUp(grid, height) && height > upTree.height
}

fun Tree.isVisibleFromDown(grid: Array<Array<Tree>>, height: Int): Boolean {
    if (isOnTheSide) return true
    val downTree = grid[row + 1][col]
    return downTree.isVisibleFromDown(grid, height) && height > downTree.height
}

fun Tree.getRightDistance(grid: Array<Array<Tree>>, height: Int): Int {
    if (isOnTheSide) return 0
    val rightTree = grid[row][col + 1]
    if (height <= rightTree.height) return 1

    return rightTree.getRightDistance(grid, height) + 1
}

fun Tree.getLeftDistance(grid: Array<Array<Tree>>, height: Int): Int {
    if (isOnTheSide) return 0
    val leftTree = grid[row][col - 1]
    if (height <= leftTree.height) return 1

    return leftTree.getLeftDistance(grid, height) + 1
}

fun Tree.getUpDistance(grid: Array<Array<Tree>>, height: Int): Int {
    if (isOnTheSide) return 0
    val upTree = grid[row - 1][col]
    if (height <= upTree.height) return 1

    return upTree.getUpDistance(grid, height) + 1
}

fun Tree.getDownDistance(grid: Array<Array<Tree>>, height: Int): Int {
    if (isOnTheSide) return 0
    val downTree = grid[row + 1][col]
    if (height <= downTree.height) return 1

    return downTree.getDownDistance(grid, height) + 1
}