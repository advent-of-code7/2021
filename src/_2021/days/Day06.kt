const val DAYS_COUNT = 257

val dp = Array(1000) { Array<Long>(1000) { -1L } }

fun countDays(day: Int, bornDay: Int): Long {
    if (bornDay >= day) return 0
    if (dp[day][bornDay] != -1L)
        return dp[day][bornDay]

    var childrenChildrenCount = 0L
    for (childBornDay in (day - bornDay) downTo 0 step 7) {
        print("$childBornDay , ")
        childrenChildrenCount += countDays(childBornDay, 9)
    }
    println()
    val result = 1 + (day - bornDay - 1) / 7 + childrenChildrenCount
    print("$day : $bornDay , ${result}")
    println()
    dp[day][bornDay] = result
    return dp[day][bornDay]
}

fun main() {

    val fishBornDay = mutableListOf<Int>()
    fun part1(input: List<String>): Int {
        val initialStates = input[0].split(",").map { it.toInt() + 1 }
        println(initialStates.sumOf { countDays(DAYS_COUNT, it) } + initialStates.size)
        return fishBornDay.size
    }


    fun part2(input: List<String>): Int {
        return input.size
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day6",2021)
    println(part1(testInput))
}
