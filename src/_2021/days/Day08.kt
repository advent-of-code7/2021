fun main() {

    val numbersKeys = hashMapOf<String, Int>(
        "abcefg" to 0,
        "cf" to 1,
        "acdeg" to 2,
        "acdfg" to 3,
        "bcdf" to 4,
        "abdfg" to 5,
        "abdefg" to 6,
        "acf" to 7,
        "abcdefg" to 8,
        "abcdfg" to 9,
    )

    fun getCount(s: String): Int {

        val abcdefg = HashMap<Char, Char>()
        val s = s.split("|")
        val l1 = s[0].split(" ").dropLast(1).map { it.toCharArray().sorted().joinToString("") }.sortedBy { it.length }
            .reversed()
        val l2 = s[1].split(" ").drop(1).map { it.toCharArray().sorted().joinToString("") }



        for (c in 'a'..'g') {
            if (l1.subList(1, 4).count { it.contains(c) } == 2 && l1.last().contains(c)) {
                abcdefg['c'] = c
                abcdefg['f'] = l1.last().first { it != c }
                abcdefg['a'] = l1.dropLast(1).last().first { it != c && it != abcdefg['f'] }
                val six = l1.subList(1, 4).first {
                    !it.contains(abcdefg['c']!!)
                }
                val five = l1.first { it.length == 5 && it.contains(abcdefg['f']!!) && !it.contains(abcdefg['c']!!) }
                for (char in 'a'..'g') {
                    if (!five.contains(char) && six.contains(char)) {
                        abcdefg['e'] = char
                        val nine = l1.first { it.length == 6 && !it.contains(char) }
                        for (input in l1) {
                            val b = input.length == 6 && input != six && input != nine
                            if (b) {
                                for (ch in 'a'..'g') {
                                    val b1 = !input.contains(ch) && six.contains(ch) && nine.contains(ch)
                                    if (b1) {
                                        abcdefg['d'] = ch
                                        abcdefg['b'] =
                                            l1.first { it.length == 4 }.first { !abcdefg.values.contains(it) }
                                        abcdefg['g'] = l1.first().first { !abcdefg.values.contains(it) }
                                        break
                                    }
                                }
                            }
                        }
                        break
                    }
                }
                break
            }
        }

        val associate = abcdefg.entries.associate { it.value to it.key }

        var number = ""
        l2.forEach {
            val mapped = it.map {
                val c = associate[it]
                c
            }.joinToString("").toCharArray().sorted().joinToString("")
            number += numbersKeys[mapped].toString()
        }


        return number.toInt()
    }

    fun part1(input: List<String>): Int {
        return input.fold(0) { acc, s ->
            acc + getCount(s)
        }
    }


    fun part2(input: List<String>): Int {
        return input.size
    }

    println(part1(readInput("day8",2021)))
}
