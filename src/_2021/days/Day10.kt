package _2021.days

import readInput
import java.util.Stack


fun main() {

    val lines = readInput("day10",2021)

    var corruptionScore = 0
    var inCompletionScores = mutableListOf<Long>()
    lines.forEach {
        val corruptionStack = Stack<Char>()
        val inCompletionStack = Stack<Char>()
        var isCorrupted = false


        it.forEach {
            if (!isCorrupted) {
                if (it in Opens.values().map { it.char }) {
                    corruptionStack.push(it)
                    inCompletionStack.push(it)
                } else {
                    if (corruptionStack.empty()) {
                        isCorrupted = true
                    } else {

                        val lastOpen = corruptionStack.pop()
                        val currentClose = Closes.findByChar(it)

                        inCompletionStack.pop()
                        if (Opens.values()[currentClose.ordinal].char != lastOpen) {
                            corruptionScore += currentClose.value
                            isCorrupted = true
                        }
                    }
                }
            }
        }

        if (!isCorrupted) {
            var inCompletionCurrentScore = 0L

            while (corruptionStack.empty().not()) {
                val lastOpenChar = corruptionStack.pop()
                val lastOpenType = Opens.values().first { it.char == lastOpenChar }


                inCompletionCurrentScore *= 5
                inCompletionCurrentScore += Closes.valueOf(lastOpenType.name).value
            }
            if (inCompletionCurrentScore > 0)
                inCompletionScores.add(inCompletionCurrentScore)
        }
    }
    inCompletionScores.sort()
    println(
        inCompletionScores[inCompletionScores.size / 2]
    )
}

enum class Opens(val char: Char) {
    PARENTHESES('('),
    SQUARE('['),
    CURLY('{'),
    ANGLE('<')
}

enum class Closes(val char: Char, val value: Int) {
    PARENTHESES(')', 1),
    SQUARE(']', 2),
    CURLY('}', 3),
    ANGLE('>', 4);


    companion object {
        fun findByChar(char: Char): Closes {
            return when (char) {
                PARENTHESES.char -> {
                    PARENTHESES
                }
                SQUARE.char -> {
                    SQUARE
                }
                CURLY.char -> {
                    CURLY
                }
                ANGLE.char -> {
                    ANGLE
                }
                else -> ANGLE
            }
        }
    }
}