fun main() {
    fun part1(input: List<String>): Int {
        return 0
    }
    fun countZerosAndOnes(list: String): Pair<Int, Int> {
        var zeros = 0
        var ones = 0
        list.forEach {
            if (it == '1')
                zeros++
            else
                ones++
        }
        return Pair(zeros, ones)
    }

    fun part2(input: List<String>): Int {
        var newInput = input
        var co2 = ""
        (input[0].indices)
            .map { index ->
                input.map { it[index] }
            }.forEachIndexed { index, list ->
                if (newInput.isEmpty()) return@forEachIndexed
                (newInput.indices).forEach {
                    countZerosAndOnes(newInput[it])
                }
//                co2 += currentMostRepetition
            }
        newInput = input
        var oxygen = ""
        (input[0].indices)
            .map { index ->
                input.map { it[index] }
            }.forEachIndexed { index, list ->
                if (newInput.isEmpty()) return@forEachIndexed
                val currentColumns = (newInput[0].indices)
                    .map { newInputIndex ->
                        newInput.map { it[newInputIndex] }
                    }
                val currentMostRepetition =
                    if ((currentColumns[index].count { it == '1' } >= newInput.size / 2 && newInput.size % 2 == 1)
                        || (currentColumns[index].count { it == '1' } > newInput.size / 2 && newInput.size % 2 == 0)
                    ) '1' else '0'
                newInput = newInput.filter {
                    it[index] == currentMostRepetition
                }
                oxygen += currentMostRepetition
            }
        println("$co2 ${Integer.parseInt(co2, 2)} $oxygen ${Integer.parseInt(oxygen, 2)}")
        val result = Integer.parseInt(co2, 2) * Integer.parseInt(oxygen, 2)
        println(result)
        return 0
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day3",2021)
    println(part1(testInput))
    println(part2(testInput))

}
