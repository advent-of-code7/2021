@file:JvmName("Day05Kt_2021")

fun main() {
    fun part1(input: List<String>): Int {
        val sums = input.windowed(3).map { it.sumOf { it.toInt() } }
        return sums.windowed(2).fold(0) { acc, list -> acc + if (list[1] > list[0]) 1 else 0 }
    }

    fun part2(input: List<String>): Int {
        return input.size
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day1.txt",2021)
    println(part1(testInput))
}
