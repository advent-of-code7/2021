fun main() {
    fun part1(input: List<String>): Int {
        val horizontal = input.map { it.split(" ") }.sumOf { if (it[0] == "forward") it[1].toInt() else 0 }
        val vertical =
            input.map { it.split(" ") }
                .sumOf {
                    when {
                        it[0] == "up" -> -it[1].toInt()
                        it[0] == "down" -> it[1].toInt()
                        else -> 0
                    }
                }
        return horizontal * vertical
    }

    fun part2(input: List<String>): Int {
        val horizontal = input.map { it.split(" ") }.sumOf { if (it[0] == "forward") it[1].toInt() else 0 }
        var vertical = 0
        input.map { it.split(" ") }
            .fold(0) { acc, it ->
                println("${it[0]} $acc ")
                when {
                    it[0] == "up" -> {
                        acc - it[1].toInt()
                    }
                    it[0] == "down" -> {
                        acc + it[1].toInt()
                    }
                    else -> {
                        vertical += acc * it[1].toInt()
                        acc
                    }
                }
            }
        return horizontal * vertical
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day2", 2021)
    println(part1(testInput))
    // test if implementation meets criteria from the description, like:
    println(part2(testInput))
}
