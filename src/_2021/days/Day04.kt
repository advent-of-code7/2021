import java.util.ArrayList

fun main() {
    val boardsChecks = arrayListOf<Array<BooleanArray>>()
    val boards = arrayListOf<Array<IntArray>>()
    val winBoards = arrayListOf<Boolean>()

    var currentBoardNumber = -1

    fun getBoard(boardStrings: ArrayList<String>): Array<IntArray> {
        val board = Array(5) {
            IntArray(5)
        }

        boardStrings.forEachIndexed { row, s ->
            s.replace("  ", " ").split(" ").filter { it.isNotEmpty() }.map {
                it.toInt()
            }.forEachIndexed { column, i ->
                board[row][column] = i
            }
        }
        return board
    }

    fun signNumber(currentNumber: Int, boards: ArrayList<Array<IntArray>>) {
        boards.forEachIndexed { boardNumber, arrayOfIntArrays ->
            arrayOfIntArrays.forEachIndexed { row, ints ->
                ints.forEachIndexed { column, i ->
                    if (i == currentNumber)
                        boardsChecks[boardNumber][row][column] = true
                }
            }
        }
    }

    fun checkBingo() {
        boardsChecks.forEachIndexed { boardNumber, arrayOfBooleanArrays ->
            arrayOfBooleanArrays.forEachIndexed { row, booleans ->
                if (!winBoards[boardNumber] && booleans.all { it }) {
                    currentBoardNumber = boardNumber
                    winBoards[boardNumber] = true
                    if (boardNumber == 99)
                        println("${winBoards[boardNumber]} XXXX")
                }
            }
            (arrayOfBooleanArrays[0].indices).forEach { index ->
                if (!winBoards[boardNumber] && arrayOfBooleanArrays.all { it[index] }) {
                    currentBoardNumber = boardNumber
                    winBoards[boardNumber] = true
                }
            }
        }
    }

    fun getResult(calledNumber: Int): Int {
        var unMarkedNumbers = 0
        boardsChecks[currentBoardNumber].forEachIndexed { row, booleans ->
            unMarkedNumbers += booleans.foldIndexed(0) { column, acc, b ->
                println(b)
                if (!b) acc + boards[currentBoardNumber][row][column] else acc
            }
        }
        println("$unMarkedNumbers $calledNumber")

        return unMarkedNumbers * calledNumber
    }

    fun part1(input: List<String>): Int {
        val numbers = input[0].split(",").map { it.toInt() }
        val boardStrings = arrayListOf<String>()
        for (i in 2 until input.size step 6) {
            boardStrings.clear()
            for (j in i..i + 4) {
                if (input.size - 1 >= i + 4) {
                    boardStrings.add(input[j])
                }
            }
            boards.add(getBoard(boardStrings))
            boardsChecks.add(Array(5) { BooleanArray(5) })
            winBoards.add(false)
        }
        run loop@{
            numbers.forEach {
                signNumber(it, boards)
                checkBingo()
                if (winBoards.all { it }) {
                    println(getResult(it))
                    println("SS")
                    return@loop
                }
            }
        }
        winBoards.forEachIndexed { index, b ->
            if (!b)
                println("$index index")
        }
        boardsChecks[91].forEach {
            println(it.toList())
        }

        return 0
    }

    fun part2(input: List<String>): Int {
        return 0
    }


    val testInput = readInput("day4",2021)
    println(part1(testInput))
    println(part2(testInput))
}
