fun main() {

    var insertionCounter = InsertionCounter()
    val insertionsRoles = mutableMapOf<String, Char>()
    val lettersCounter = LettersCounter()

    fun part1(input: List<String>): Int {

        val template = input.first()
        input.drop(2).forEach {
            it.split(" -> ").also {
                insertionsRoles[it.first()] = it.last().first()
            }
        }

        template.windowed(2).forEach {
            insertionCounter.increaseInsertion(it, 1)
        }
        template.forEach {
            lettersCounter.increaseChar(it, 1)
        }

        println(insertionCounter)
        println()

        repeat(40) {
            val tempInsertionCounter = InsertionCounter()
            insertionCounter.forEach { insertion, count ->
                if (insertionsRoles.contains(insertion)) {
                    lettersCounter.increaseChar(insertionsRoles[insertion]!!, count)
                    tempInsertionCounter.increaseInsertion("${insertion.first()}${insertionsRoles[insertion]}", count)
                    tempInsertionCounter.increaseInsertion("${insertionsRoles[insertion]}${insertion.last()}", count)
                    tempInsertionCounter.decreaseInsertion(insertion, count)
                }
            }

            tempInsertionCounter.forEach {
                insertionCounter[it.key] = insertionCounter[it.key]?.plus(it.value) ?: it.value
            }
            println(tempInsertionCounter)
            println(insertionCounter)
            println(lettersCounter)
            println()
        }
        lettersCounter.values.sorted().also {
            println(it.last() - it.first())
        }


        return 0
    }

    fun part2(input: List<String>): Int {
        return input.size
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day14",2021)
    println(part1(testInput))
}

class InsertionCounter : HashMap<String, Long>() {

    fun increaseInsertion(insertion: String, count: Long) {
        set(insertion, get(insertion)?.plus(count) ?: count)
    }

    fun decreaseInsertion(insertion: String, count: Long) {
        set(insertion, get(insertion)?.minus(count) ?: -count)
    }
}


class LettersCounter : HashMap<Char, Long>() {

    fun increaseChar(insertion: Char, count: Long) {
        set(insertion, get(insertion)?.plus(count) ?: count)
    }
}