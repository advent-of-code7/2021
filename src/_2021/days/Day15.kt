import java.util.*

val caveRisks: MutableList<MutableList<Int>> = mutableListOf()

fun main() {

    val cellsRiskLevels = Array(2000) { x ->
        Array(2000) { y ->
            Int.MAX_VALUE
        }
    }

    val visitedCells = Array(2000) {
        Array(2000) { false }
    }

    fun getAdjacent(location: Pair<Int, Int>): List<Pair<Int, Int>> = listOf(
        location.first - 1 to location.second,
        location.first + 1 to location.second,
        location.first to location.second - 1,
        location.first to location.second + 1,
    )

    fun dijkstra(start: Pair<Int, Int>): Int {

        val priorityQueue = PriorityQueue(
            compareBy<Pair<Int, Int>> {
                cellsRiskLevels[it.first][it.second]
            }
        )

        cellsRiskLevels[start.first][start.second] = 0
        visitedCells[start.first][start.second] = true

        priorityQueue.add(start)

        while (priorityQueue.isNotEmpty()) {
            val currentLocation = priorityQueue.remove()
            val currentLocationRisk = cellsRiskLevels[currentLocation.first][currentLocation.second]
            visitedCells[currentLocation.first][currentLocation.second] = true
//            println(priorityQueue)

            getAdjacent(currentLocation).forEach {
                if (it.first !in 0 until caveRisks.size || it.second !in 0 until caveRisks.first().size) {
                    //DON'T DO ANYTHING
                } else if (visitedCells[it.first][it.second].not()) {
                    if (cellsRiskLevels[it.first][it.second] > currentLocationRisk + caveRisks[it.first][it.second]) {
                        cellsRiskLevels[it.first][it.second] = currentLocationRisk + caveRisks[it.first][it.second]
                    }

                    if (priorityQueue.contains(it).not())
                        priorityQueue.add(it)
                }
            }
        }

        return 0
    }

    fun part1(input: List<String>): Int {
        val initialCaveRisks = input.fold(mutableListOf<MutableList<Int>>()) { acc, s ->
            acc.add(
                s.fold(mutableListOf()) { intAcc, char ->
                    intAcc.add(char.digitToInt())
                    intAcc
                }
            )
            acc
        }

        val tempCaveRisks = mutableListOf<MutableList<Int>>()
        initialCaveRisks.forEach {
            val initialRow = it
            val extendedRow = mutableListOf<Int>()
            repeat(5) { index ->
                extendedRow.addAll(
                    initialRow.map {
                        if ((index + it) > 9) (index + it) % 9 else (index + it)
                    }
                )
            }
            tempCaveRisks.add(extendedRow)
        }
        repeat(5) { index ->
            caveRisks.addAll(
                tempCaveRisks.map {
                    it.map {
                        if ((index + it) > 9) (index + it) % 9 else (index + it)
                    }.toMutableList()
                }
            )
        }

        dijkstra(Pair(0, 0)).also {
            println(cellsRiskLevels[caveRisks.lastIndex][caveRisks.first().lastIndex])
        }

        return 0
    }

    fun part2(input: List<String>): Int {
        return input.size
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day15",2021)
    println(part1(testInput))
}