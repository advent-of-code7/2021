import kotlin.math.abs


fun main() {

    fun part1(input: List<String>): Int {
        val positions = input[0].split(",").map { it.toInt() }.sorted()
        val minNumber = positions.first()
        val maxNumber = positions.last()
        val positionCounts = mutableMapOf<Int, Int>()
        positions.forEach {
            positionCounts[it] = positionCounts[it]?.plus(1) ?: 1
        }

        val range = (minNumber..maxNumber).toList()

        var x = 0
        val min = range.minOf { selectedNumber ->
            x = 0
            positionCounts.forEach { entry ->
                val currentSum = if (entry.key == selectedNumber) 0
                else {
                    val n = abs(entry.key - selectedNumber)
                    (n * (n + 1)) / 2
                }
                x += currentSum * entry.value

                print(" ${entry.key}=$currentSum , ")
            }
            println()
            print("$selectedNumber , $x")
            println()
            x
        }

        println(min)

        return 0
    }


    fun part2(input: List<String>): Int {
        return input.size
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day7",2021)
    println(part1(testInput))
}
