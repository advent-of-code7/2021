val visited = Array(10) { Array(10) { false } }
lateinit var grid: MutableList<MutableList<Int>>
fun main() {
    fun resetBiggerThan9() {
        grid.forEachIndexed { row, mutableList ->
            mutableList.forEachIndexed { col, i ->
                visited[row][col] = false
                if (i > 9) {
                    grid[row][col] = 0
                }
            }
        }
    }

    fun part1(input: List<String>): Int {
        grid = input.fold(mutableListOf<MutableList<Int>>()) { acc, s ->

            val numbers = s.fold(mutableListOf<Int>()) { ints, c ->
                ints.add(c.digitToInt())
                ints
            }

            acc.add(numbers)
            acc
        }

        var counter = 0
        var round = 0
        while (true) {

            round++
            grid.forEachIndexed { row, mutableList ->
                mutableList.forEachIndexed { col, i ->
                    grid[row][col]++
                }
            }

            grid.forEachIndexed { rowIndex, row ->
                row.forEachIndexed { col, number ->
                    if (number > 9) {
                        explodingCell(rowIndex, col)
                    }
                }
            }

            visited.forEachIndexed { row, booleans ->
                booleans.forEachIndexed { col, b ->
                    if (b)
                        counter++
                }
            }

            if (
                visited.all { it.all { it } }
            ) {
                println("First synchronizing flashes $round")
                break
            }

            resetBiggerThan9()

        }

        return counter
    }

    fun part2(input: List<String>): Int {
        return input.size
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day11",2021)
    println(part1(testInput))
}


fun explodingCell(row: Int, col: Int) {
    if (row < 0 || row > 9 || col < 0 || col > 9) return
    if (visited[row][col]) return


    visited[row][col] = true

    getNeighborhoods(row, col).forEach {
        if (it.first < 0 || it.first > 9 || it.second < 0 || it.second > 9) return@forEach

        grid[it.first][it.second]++
    }

    getNeighborhoods(row, col).forEach {
        if (it.first < 0 || it.first > 9 || it.second < 0 || it.second > 9) return@forEach

        if (grid[it.first][it.second] > 9) {
            explodingCell(it.first, it.second)
        }
    }

}

fun getNeighborhoods(row: Int, col: Int): List<Pair<Int, Int>> {
    return listOf(
        row - 1 to col,
        row - 1 to col + 1,
        row - 1 to col - 1,
        row to col - 1,
        row to col + 1,
        row + 1 to col,
        row + 1 to col + 1,
        row + 1 to col - 1
    )
}