import java.lang.Integer.max
import kotlin.math.abs
import kotlin.math.min

fun main() {
    var counter = 0

    data class Point(val x: Int, val y: Int)
    data class Line(val point1: Point, val point2: Point) {
        fun getLinePoints(): List<Point> {
            val list = arrayListOf<Point>()
            println("$point1 $point2")
            if (point1.x == point2.x) {
                for (i in min(point1.y, point2.y)..max(point1.y, point2.y)) {
                    list.add(Point(point1.x, i))
                }
            } else if (point1.y == point2.y) {
                for (i in min(point1.x, point2.x)..max(point1.x, point2.x)) {
                    list.add(Point(i, point1.y))
                }
            } else if (point1.x == point1.y && point2.x == point2.y) {
                for (i in 0..(abs(point1.x - point2.x))) {
                    list.add(Point(point1.x + i, point1.y + i))
                }
            } else if (point1.x == point2.y && point1.y == point2.x) {
                for (i in 0..(abs(point1.x - point2.x))) {
                    if (point1.x < point2.x && point1.y < point2.y) {
                        list.add(
                            Point(
                                point1.x + i, point1.y + i
                            )
                        )
                    } else if (point1.x < point2.x && point1.y > point2.y) {
                        list.add(
                            Point(
                                point1.x + i, point1.y - i
                            )
                        )
                    } else if (point1.x > point2.x && point1.y < point2.y) {
                        list.add(
                            Point(
                                point1.x - i, point1.y + i
                            )
                        )
                    } else if (point1.x > point2.x && point1.y > point2.y) {
                        list.add(
                            Point(
                                point1.x - i, point1.y - i
                            )
                        )
                    }
                }
            }
            println("$list ${list.size}")
            return list
        }
    }

    val board = Array(10) {
        IntArray(10)
    }

    fun drawLine(line: Line) {
        line.getLinePoints().forEach {
            println("${it.x} ${it.y}")
            board[it.x][it.y]++
            println(board[3][4])
            if (board[it.x][it.y] == 2) counter++
        }
    }

    fun part1(input: List<String>): Int {
        input.forEach { s ->
            val coordinates = s.split(" ").filter { it != "->" }.map { it.split(",").map { it.toInt() } }.flatten()
            drawLine(Line(Point(coordinates[0], coordinates[1]), Point(coordinates[2], coordinates[3])))
        }
        board.forEachIndexed { row, ints ->
            ints.forEachIndexed { column, i ->
                print("row:$row column:$column $$i ")
            }
            println()
        }

        board.forEachIndexed { row, ints ->
            ints.forEachIndexed { column, i ->
                print("$i ")
            }
            println()
        }

        return counter
    }

    fun part2(input: List<String>): Int {
        return 0
    }


    val testInput = readInput("day5",2021)
    println(part1(testInput))
    println(part2(testInput))
}
