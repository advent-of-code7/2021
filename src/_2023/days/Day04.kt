package _2023.days

import readInput
import java.util.*
import kotlin.math.pow

fun main() {

    fun part1(input: List<String>): String {
        val total = input.fold(0) { acc, line ->
            val splits = line.dropWhile { it != ':' }.drop(1).split("|")
            val leftPart = splits[0].split(" ").mapNotNull { it.toIntOrNull() }
            val rightPart = splits[1].split(" ").mapNotNull { it.toIntOrNull() }.associateWith {
                1
            }
            val lineResult = leftPart.count { number ->
                rightPart.containsKey(number)
            }.takeIf { it > 0 }?.let { 2.0.pow((it.toDouble() - 1).coerceAtLeast(0.0)) }
            acc + (lineResult?.toInt() ?: 0)
        }
        return "$total"
    }

    fun part2(input: List<String>): String {
        val cardsCount = mutableMapOf<Int, Int>(
            *input.indices.map { it to 1 }.toTypedArray()
        )
        input.forEachIndexed { index, line ->
            val splits = line.dropWhile { it != ':' }.drop(1).split("|")
            val leftPart = splits[0].split(" ").mapNotNull { it.toIntOrNull() }
            val rightPart = splits[1].split(" ").mapNotNull { it.toIntOrNull() }.associateWith {
                1
            }

            leftPart.count { number ->
                rightPart.containsKey(number)
            }.also { matchesCount ->
                repeat(matchesCount) {
                    cardsCount[index + it + 1] =
                        (cardsCount[index + it + 1] ?: 1) + (1 * cardsCount.getOrDefault(index, 1))
                }
            }
        }

        return "${cardsCount.values.sum()}"
    }


    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day4", 2023)
    println(part1(testInput))
    println(part2(testInput))
}
