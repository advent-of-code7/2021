package _2023.days

import readInput

fun main() {

    fun part1(input: List<String>): String {
        val total = input.fold(0) { acc, line ->
            val splits = line.replace(":", "")
                .replace(",", "")
                .replace(";", "")
                .split(" ")
            val gameId = splits.first {
                it.toIntOrNull() != null
            }.toInt()

            val isAllValid = splits.drop(2).chunked(2) {
                val number = it.first().toInt()
                val color = it.last()
                color == "red" && number <= 12 ||
                        (color == "green" && number <= 13) ||
                        (color == "blue" && number <= 14)
            }.all { it }

            if (isAllValid)
                acc + gameId
            else
                acc
        }
        return "$total"
    }

    fun part2(input: List<String>): String {
        val total = input.fold(0) { acc, line ->
            val splits = line.replace(":", "")
                .replace(",", "")
                .replace(";", "")
                .split(" ")


            val maxes = splits.drop(2)
                .chunked(2)
                .fold(Triple(1, 1, 1))
                { lineAcc, (numberString, color) ->
                    val number = numberString.toInt()
                    when (color) {
                        "red" -> {
                            lineAcc.copy(maxOf(lineAcc.first, number))
                        }

                        "green" -> {
                            lineAcc.copy(second = maxOf(lineAcc.second, number))
                        }

                        else -> {
                            lineAcc.copy(third = maxOf(lineAcc.third, number))
                        }
                    }
                }
            acc + maxes.run { first * second * third }.also { println(it) }
        }
        return "$total"
    }


    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day2", 2023)
    println(part1(testInput))
    println(part2(testInput))
}
