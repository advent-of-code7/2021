package _2023.days

import readInput

fun main() {

    fun readMappers(mappersInformation: List<String>): Map<String, Mapper> {
        val mappers: MutableMap<String, Mapper> = mutableMapOf()
        mappersInformation.map { it.lines() }.map { titleWithRanges ->
            val (source, destination) = titleWithRanges.first()
                .split(" ")
                .first()
                .split("-")
                .run {
                    first() to last()
                }
            val ranges = titleWithRanges.drop(1).map {
                it.split(" ").map { it.toLong() }.run {
                    Range(first(), get(1), last())
                }
            }

            mappers[source] = Mapper(destination, ranges)
        }
        return mappers.toMap()
    }

    fun part1(input: List<String>): String {

        val min = input.joinToString("\n").split("\n\n").let { dataGroups ->
            val seeds = dataGroups.first().split(" ").drop(1).map { it.toLong() }
            val mappers = readMappers(dataGroups.drop(1))

            val min = seeds.minOf {
                mappers["seed"]!!.mapToNext(it, mappers)
            }
            min
        }

        return min.toString()
    }

    fun part2(input: List<String>): String {

        val min = input.joinToString("\n").split("\n\n").let { dataGroups ->
            val seeds =
                dataGroups.first().split(" ").drop(1)
                    .zipWithNext { a, b -> LongRange(a.toLong(), a.toLong() + b.toLong()-1) }
            val mappers = readMappers(dataGroups.drop(1))

            val min = seeds.minOf {
                it.minOf {
                    mappers["seed"]!!.mapToNext(it, mappers)
                }
            }
            min
        }

        return min.toString()
    }


    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day5", 2023)
    println(part1(testInput))
    println(part2(testInput))
}

data class Mapper(val destinationName: String, val ranges: List<Range>) {
    fun mapToNext(value: Long, mappers: Map<String, Mapper>): Long {
        val range = ranges.find { value in it.sourceStart..(it.sourceStart + it.length) }
        val mappedValue = if (range == null) {
            value
        } else {
            val padding = range.destinationStart - range.sourceStart
            value + padding
        }
        return mappers[destinationName]?.mapToNext(mappedValue, mappers) ?: mappedValue
    }
}

data class Range(val destinationStart: Long, val sourceStart: Long, val length: Long)