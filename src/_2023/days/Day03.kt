package _2023.days

import readCharArray2d
import readInput

fun main() {

    fun part1(input: List<String>): String {
        val matrix = readCharArray2d(input)
        val total = matrix.foldIndexed(0) { row, linesAcc, chars ->
            val numbers = "\\d+".toRegex().findAll(chars.concatToString())

            val lineTotal = numbers.fold(0) { acc, matchResult ->
                val isValidNumber = matchResult.range.any { charIndex ->
                    checkAround(matrix, row, charIndex)
                    true
                }

                if (isValidNumber) {
                    acc + matchResult.value.toInt()
                } else
                    acc
            }

            linesAcc + lineTotal
        }
        return "$total"
    }

    fun part2(input: List<String>): String {
        val matrix = readCharArray2d(input)
        val total = matrix.foldIndexed(0) { row, linesAcc, chars ->
            val numbers = "\\d+".toRegex().findAll(chars.concatToString())

            val lineTotal = numbers.fold(mapOf<Pair<Int, Int>, List<Int>>()) { acc, matchResult ->
//                val isValidNumber = matchResult.range.any { charIndex ->
//                    checkAround(matrix, row, charIndex)
//                }
//
//                if (isValidNumber) {
//                    acc.getOrDefault(row)
//                } else
                    acc
            }

            linesAcc
        }
        return ""
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day3", 2023)
    println(part1(testInput))
    println(part2(testInput))
}

fun checkAround(matrix: Array<CharArray>, row: Int, col: Int): List<Pair<Pair<Int, Int>, Char>> {
    return arrayOf(
        Pair(row - 1, col - 1),
        Pair(row - 1, col),
        Pair(row - 1, col + 1),
        Pair(row, col - 1),
        Pair(row, col + 1),
        Pair(row + 1, col - 1),
        Pair(row + 1, col),
        Pair(row + 1, col + 1)
    )
        .filter { (r, c) ->
            r in matrix.indices && c in matrix[0].indices
        }
        .mapNotNull { (r, c) ->
            val value = matrix[r][c]
            if (!value.isDigit() && value != '.') {
                Pair(Pair(r, c), value)
            } else {
                null
            }
        }
}