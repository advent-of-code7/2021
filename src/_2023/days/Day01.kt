package _2023.days

import readInput
import java.util.*

fun main() {

    fun part1(input: List<String>): String {
        val total = input.fold(0) { acc, line ->
            val firstDigit = line.first { it.isDigit() }
            val lastDigit = line.last { it.isDigit() }
            val lineNumber = "$firstDigit$lastDigit".toInt()
            acc + lineNumber
        }
        return "$total"
    }

    fun part2(input: List<String>): String {
        val numbers = listOf(
            "one" to 1,
            "two" to 2,
            "three" to 3,
            "four" to 4,
            "five" to 5,
            "six" to 6,
            "seven" to 7,
            "eight" to 8,
            "nine" to 9,
            "1" to 1,
            "2" to 2,
            "3" to 3,
            "4" to 4,
            "5" to 5,
            "6" to 6,
            "7" to 7,
            "8" to 8,
            "9" to 9
        )

        val total = input.fold(0) { acc, line ->

            val (_, firstDigit) = numbers.fold(Int.MAX_VALUE to -1) { acc, number ->
                val firstExistence = line.indexOf(number.first)
                if (firstExistence == -1) {
                    acc
                } else {
                    if (acc.first > firstExistence) {
                        firstExistence to number.second
                    } else
                        acc
                }
            }

            val (_, lastDigit) = numbers.fold(Int.MIN_VALUE to -1) { acc, number ->
                val firstExistence = line.lastIndexOf(number.first)
                if (firstExistence == -1) {
                    acc
                } else {
                    if (acc.first < firstExistence) {
                        firstExistence to number.second
                    } else
                        acc
                }
            }
            println(line)
            val lineNumber = "$firstDigit$lastDigit".toInt()
            acc + lineNumber
        }
        return "$total"
    }


    // test if implementation meets criteria from the description, like:
    val testInput = readInput("day1", 2023)
    println(part1(testInput))
    println(part2(testInput))
}
